/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2021, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

 /** \file     StreamMergeAppCfg.cpp
     \brief    Decoder configuration class
 */

#include <cstdio>
#include <cstring>
#include <string>
#include "StreamMergeAppCfg.h"
#include "Utilities/program_options_lite.h"


using namespace std;
namespace po = df::program_options_lite;

//! \ingroup DecoderApp
//! \{

// ====================================================================================================================
// Public member functions
// ====================================================================================================================

/** \param argc number of arguments
    \param argv array of arguments
 */
bool StreamMergeAppCfg::parseCfg(int argc, char* argv[])
{
  // Check all arguments.
  m_numInputStreams = 0;
  for (int i = 1; i < argc; i++) {
    string optionValue = argv[i];
    
    if (optionValue == "--allLayersInOneOls")
    {
      m_allLayersInOneOls = true;
    }
    else if (optionValue == "--layergrid")
    {
      m_layerGridConfiguration = new LayerGridConfiguration;
      m_allLayersInOneOls   = true;
      // Verify minimum argc.
      if (argc - 1 < i + (m_numInputStreams - 1) * 4) {
        return false;
      }
      // Loop over layer grid configuration arguments. Because we need m_numInputStreams, this must
      //  be passed to the cmd line arguments AFTER the input and output bitstreams.
      for (LayerID layerGridLayerId = 0; layerGridLayerId < m_numInputStreams - 1; layerGridLayerId++, i += 4)
      {
        PieceBoundaryConfiguration *pieceBC = m_layerGridConfiguration->getPieceBoundaryConfiguration(layerGridLayerId);
        pieceBC->setBoundaryIds(
          std::stoi(argv[i + 1]),
          std::stoi(argv[i + 2]),
          std::stoi(argv[i + 3]),
          std::stoi(argv[i + 4])
        );
      }

      try
      {
        m_layerGridConfiguration->testBoundaryConfigurations();
      }
      catch (Exception e)
      {
        msg(ERROR, e.what());
        return false;
      }
    }
    else
    {
      // This includes the last argument (the output bitstream).
      m_bitstreamFileNameIn[m_numInputStreams] = argv[i];
      m_numInputStreams++;
    }
  }
  // Handle last argument (the output bitstream).
  m_numInputStreams--;
  m_bitstreamFileNameOut = m_bitstreamFileNameIn[m_numInputStreams];
  m_bitstreamFileNameIn[m_numInputStreams] = "";
  
  return true;
}

StreamMergeAppCfg::StreamMergeAppCfg()
  : m_bitstreamFileNameOut()
  , m_numInputStreams(0)
  , m_allLayersInOneOls(false)
  , m_layerGridConfiguration(nullptr)
{
  for (int i = 0; i < MAX_VPS_LAYERS; i++)
    m_bitstreamFileNameIn[i] = "";
}

StreamMergeAppCfg::~StreamMergeAppCfg()
{

}

//! \}
