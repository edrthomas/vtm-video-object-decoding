/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     LayerGrid.cpp
    \brief    layer grid configuration functionality
*/


#include <unordered_set>
#include "LayerGrid.h"

using namespace std;

void LayerGridConfiguration::reset()
{
  for (auto& entry: pieces)
    delete entry.second;
  pieces.clear();
  for (auto& entry: colorChannelDimensions)
  {
    entry.second.width  = 0;
    entry.second.height = 0;
  }
};

void LayerGridConfiguration::finalizeIfChanged(LayerIdToPicture layerIdToPicture)
{
  // Check if any JBC has changed. If not, return.
  if (!someHaveChanged(m_pieceBoundaryConfigurations))
  {
    return;
  }

  // Reset.
  reset();

  // Create collection of layer grid pieces.
  pieces = collectPieces(m_pieceBoundaryConfigurations, layerIdToPicture);

  // Assemble pieces.
  pair<BoundaryIdToLayerIdPair, BoundaryIdToLayerIdPair> boundaryMaps = reverseMap(m_pieceBoundaryConfigurations);
  BoundaryIdToLayerIdPair                                hBoundaryIdToLayerIdsPlus1 = boundaryMaps.first;
  BoundaryIdToLayerIdPair                                vBoundaryIdToLayerIdsPlus1 = boundaryMaps.second;
  assemble(&pieces, &m_pieceBoundaryConfigurations, &hBoundaryIdToLayerIdsPlus1, &vBoundaryIdToLayerIdsPlus1);


  // Get coordinates of each piece relative to a reference piece. This throws if a piece doesn't have neighbors.
  LayerGridPiece *referencePiece = pieces.begin()->second;
  setRelativePositions(pieces, referencePiece);

  // Get layer grid dimensions. This also sets the absolute position of the reference piece.
  colorChannelDimensions = findDimensionsAndReferencePosition(pieces, referencePiece);

  setAbsolutePositions(pieces, referencePiece, colorChannelDimensions);
  
  // For each JBC, resetIsChanged()
  for (auto &entry: m_pieceBoundaryConfigurations)
  {
    entry.second->resetIsChanged();
  }
}

bool LayerGridConfiguration::someHaveChanged(LayerIdToPieceBoundaryConfiguration layerIdToJBConfig)
{
  bool someHaveChanged = false;
  for (auto& entry: layerIdToJBConfig)
  {
    PieceBoundaryConfiguration *jbConfig = entry.second;
    if (jbConfig->getIsChanged())
    {
      someHaveChanged = true;
      break;
    }
  }
  return someHaveChanged;
}

// Initialize the LayerGridPiece instances.
LayerIdToLayerGridPiece LayerGridConfiguration::collectPieces(LayerIdToPieceBoundaryConfiguration layerIdToJBConfig,
                                                              LayerIdToPicture                    layerIdToPicture)
{
  LayerIdToLayerGridPiece pieces;
  for (auto& entry: layerIdToJBConfig)
  {
    LayerID           layerId     = entry.first;
    PelUnitBuf const &layerBuffer = layerIdToPicture[layerId]->getRecoBuf();

    LayerGridPiece *layerGridPiece = new LayerGridPiece;
    pieces[layerId]                = layerGridPiece;
    layerGridPiece->layerId        = layerId;
    // Set proper sizes per color channel.
    for (int cc = 0; cc < getNumberValidComponents(layerBuffer.chromaFormat); cc++)
    {
      layerGridPiece->colorChannelDimensions[cc].width  = layerBuffer.bufs[cc].width;
      layerGridPiece->colorChannelDimensions[cc].height = layerBuffer.bufs[cc].height;
    }
  }
  return pieces;
}

// Set the neighbors of each LayerGridPiece.
void LayerGridConfiguration::assemble(LayerIdToLayerGridPiece *            pieces,
                                      LayerIdToPieceBoundaryConfiguration *layerIdToJBConfig,
                                      BoundaryIdToLayerIdPair *            hBoundaryIdToLayerIdsPlus1,
                                      BoundaryIdToLayerIdPair *            vBoundaryIdToLayerIdsPlus1)
{
  for (auto& entry: (*layerIdToJBConfig))
  {
    LayerID                     layerId  = entry.first;
    PieceBoundaryConfiguration *jbConfig = entry.second;

    LayerGridPiece *layerGridPiece = (*pieces)[layerId];
    if (jbConfig->getBoundaryIdNorth() > 0)
    {
      if (vBoundaryIdToLayerIdsPlus1->find(jbConfig->getBoundaryIdNorth()) == vBoundaryIdToLayerIdsPlus1->end()) {
        THROW("unknown neighbor");
      }
      LayerID neighboringLayerIdPlus1 = (*vBoundaryIdToLayerIdsPlus1)[jbConfig->getBoundaryIdNorth()].first;
      if (neighboringLayerIdPlus1 == 0) THROW("unknown neighbor");
      layerGridPiece->neighborNorth = (*pieces)[neighboringLayerIdPlus1 - 1];
    }
    if (jbConfig->getBoundaryIdEast() > 0)
    {
      if (hBoundaryIdToLayerIdsPlus1->find(jbConfig->getBoundaryIdEast()) == hBoundaryIdToLayerIdsPlus1->end())
      {
        THROW("unknown neighbor");
      }
      LayerID neighboringLayerIdPlus1 = (*hBoundaryIdToLayerIdsPlus1)[jbConfig->getBoundaryIdEast()].second;
      if (neighboringLayerIdPlus1 == 0) THROW("unknown neighbor");
      layerGridPiece->neighborEast = (*pieces)[neighboringLayerIdPlus1 - 1];
    }
    if (jbConfig->getBoundaryIdSouth() > 0)
    {
      if (vBoundaryIdToLayerIdsPlus1->find(jbConfig->getBoundaryIdSouth()) == vBoundaryIdToLayerIdsPlus1->end())
      {
        THROW("unknown neighbor");
      }
      LayerID neighboringLayerIdPlus1 = (*vBoundaryIdToLayerIdsPlus1)[jbConfig->getBoundaryIdSouth()].second;
      if (neighboringLayerIdPlus1 == 0) THROW("unknown neighbor");
      layerGridPiece->neighborSouth = (*pieces)[neighboringLayerIdPlus1 - 1];
    }
    if (jbConfig->getBoundaryIdWest() > 0)
    {
      if (hBoundaryIdToLayerIdsPlus1->find(jbConfig->getBoundaryIdWest()) == hBoundaryIdToLayerIdsPlus1->end())
      {
        THROW("unknown neighbor");
      }
      LayerID neighboringLayerIdPlus1 = (*hBoundaryIdToLayerIdsPlus1)[jbConfig->getBoundaryIdWest()].first;
      if (neighboringLayerIdPlus1 == 0) THROW("unknown neighbor");
      layerGridPiece->neighborWest = (*pieces)[neighboringLayerIdPlus1 - 1];
    }
  }
}

// Throws if the total amount of pieces is higher than the traversed amount of pieces.
// Relative positions are relative to the reference piece.
void LayerGridConfiguration::setRelativePositions(LayerIdToLayerGridPiece pieces, LayerGridPiece *referencePiece)
{
  // Initialization.
  list<LayerGridPiece *>          queue;
  unordered_set<LayerGridPiece *> traversed;

  // Start with any piece.
  queue.push_back(referencePiece);

  while (!queue.empty()) {
    // Get the next piece from the queue.
    LayerGridPiece *currentPiece = queue.front();
    queue.pop_front();

    // Get neighboring pieces.
    LayerGridPiece *neighbors[4]  = { currentPiece->neighborNorth, currentPiece->neighborEast,
                                     currentPiece->neighborSouth, currentPiece->neighborWest };
    LayerGridPiece *previousPiece = nullptr;
    for (auto neighbor: neighbors)
    {
      if (neighbor && traversed.count(neighbor) == 0)
      {
        queue.push_back(neighbor);
      }
      if (neighbor && traversed.count(neighbor) == 1) {
        previousPiece = neighbor;
      }
    }

    // Do the work.
    setRelativePosition(currentPiece, previousPiece, referencePiece);
    // Indicate we've done the work.
    traversed.insert(currentPiece);
  }
  if (pieces.size() > traversed.size()) {
    throw new Exception("invalid layer grid configuration: one or more layers are spatially orphanaged (don't have a neighbor)");
  }
}

// Relative to the reference piece.
void LayerGridConfiguration::setRelativePosition(LayerGridPiece *piece, LayerGridPiece *previousPiece,
                                                 LayerGridPiece *referencePiece)
{
  if (!previousPiece)
  {
    for (auto it: referencePiece->colorChannelDimensions)
    {
      int colorChannelIndex = it.first;
      piece->relativeColorChannelPositions[colorChannelIndex] = Position(0, 0);
    }
    return;
  }

  for (auto it: referencePiece->colorChannelDimensions)
  {
    int      colorChannelIndex     = it.first;
    Size     previousPieceSize     = it.second;
    Position previousPiecePosition = previousPiece->relativeColorChannelPositions[colorChannelIndex];
    Size     currentPieceSize      = piece->colorChannelDimensions[colorChannelIndex];

    Position relativeOffset;
    if (previousPiece->neighborSouth == piece)
    {
      relativeOffset = Position(0, previousPieceSize.height);
    }
    if (previousPiece->neighborWest == piece)
    {
      relativeOffset = Position(-1 * currentPieceSize.width, 0);
    }
    if (previousPiece->neighborNorth == piece)
    {
      relativeOffset = Position(0, -1 * currentPieceSize.height);
    }
    if (previousPiece->neighborEast == piece)
    {
      relativeOffset = Position(previousPieceSize.width, 0);
    }

    piece->relativeColorChannelPositions[colorChannelIndex] = previousPiecePosition.offset(relativeOffset);
  }
}

// This also sets the absolute position of the reference piece.
ChannelSizes LayerGridConfiguration::findDimensionsAndReferencePosition(LayerIdToLayerGridPiece pieces,
                                                                        LayerGridPiece *        referencePiece)
{
  ChannelSizes colorChannelDimensions;
  for (auto it: referencePiece->colorChannelDimensions)
  {
    int                         colorChannelIndex = it.first;
    pair<int, LayerGridPiece *> leftX, rightX, topY, bottomY;

    for (auto itp: pieces)
    {
      LayerGridPiece *piece            = itp.second;
      Position        relativePosition = piece->relativeColorChannelPositions[colorChannelIndex];
      Size            pieceSize        = piece->colorChannelDimensions[colorChannelIndex];
      

      leftX  = !leftX.second || relativePosition.x < leftX.first ? make_pair(relativePosition.x, piece) : leftX;
      topY   = !topY.second || relativePosition.y < topY.first ? make_pair(relativePosition.y, piece) : topY;
      rightX = !rightX.second
                   || (relativePosition.x + pieceSize.width)
                        > (rightX.first + rightX.second->colorChannelDimensions[colorChannelIndex].width)
                 ? make_pair(relativePosition.x, piece)
                 : rightX;
      bottomY = !bottomY.second
                    || (relativePosition.y + pieceSize.height)
                         > (bottomY.first + bottomY.second->colorChannelDimensions[colorChannelIndex].height)
                  ? make_pair(relativePosition.y, piece)
                  : bottomY;
    }
    colorChannelDimensions[colorChannelIndex].width =
      (rightX.first + rightX.second->colorChannelDimensions[colorChannelIndex].width) - leftX.first;
    colorChannelDimensions[colorChannelIndex].height =
      (bottomY.first + bottomY.second->colorChannelDimensions[colorChannelIndex].height) - topY.first;

    // Now set the absolute position of the reference piece.
    referencePiece->colorChannelPositions[colorChannelIndex] =
      Position(referencePiece->relativeColorChannelPositions[colorChannelIndex].x   // = 0
                 - leftX.second->relativeColorChannelPositions[colorChannelIndex].x,
               referencePiece->relativeColorChannelPositions[colorChannelIndex].y   // = 0
                 - topY.second->relativeColorChannelPositions[colorChannelIndex].y);
  }
  return colorChannelDimensions;
}

void LayerGridConfiguration::setAbsolutePositions(LayerIdToLayerGridPiece pieces, LayerGridPiece *referencePiece,
                                                  ChannelSizes colorChannelDimensions)
{
  for (auto it: referencePiece->colorChannelDimensions)
  {
    int colorChannelIndex = it.first;

    for (auto itp: pieces)
    {
      LayerGridPiece *piece = itp.second;

      piece->colorChannelPositions[colorChannelIndex] = referencePiece->colorChannelPositions[colorChannelIndex].offset(
        piece->relativeColorChannelPositions[colorChannelIndex]);
    }
  }
}

pair<BoundaryIdToLayerIdPair, BoundaryIdToLayerIdPair>
  LayerGridConfiguration::reverseMap(LayerIdToPieceBoundaryConfiguration layerIdToJBConfig)
{ 
  // Map boundary ID to pair of layer ID. There's a separation between horizontal and vertical boundary IDs.
  BoundaryIdToLayerIdPair hBoundaryIdToLayerIds;   // Layer ID order: left right.
  BoundaryIdToLayerIdPair vBoundaryIdToLayerIds;   // Layer ID order: top bottom.

  for (auto& entry: layerIdToJBConfig)
  {
    LayerID                     layerId  = entry.first;
    PieceBoundaryConfiguration *jbConfig = entry.second;

    BoundaryID boundaryID;
    boundaryID = jbConfig->getBoundaryIdNorth();
    if (boundaryID > 0)
    {
      vBoundaryIdToLayerIds[boundaryID].second = layerId + 1;   // +1 so the default value of 0 means no layer ID.
    }
    boundaryID = jbConfig->getBoundaryIdEast();
    if (boundaryID > 0)
    {
      hBoundaryIdToLayerIds[boundaryID].first = layerId + 1;
    }
    boundaryID = jbConfig->getBoundaryIdSouth();
    if (boundaryID > 0)
    {
      vBoundaryIdToLayerIds[boundaryID].first = layerId + 1;
    }
    boundaryID = jbConfig->getBoundaryIdWest();
    if (boundaryID > 0)
    {
      hBoundaryIdToLayerIds[boundaryID].second = layerId + 1;
    }
  }

  return make_pair(hBoundaryIdToLayerIds, vBoundaryIdToLayerIds);
}

void LayerGridConfiguration::testBoundaryConfigurations()
{
  pair<BoundaryIdToLayerIdPair, BoundaryIdToLayerIdPair> boundaryMaps = reverseMap(m_pieceBoundaryConfigurations);
  BoundaryIdToLayerIdPair                                hBoundaryIdToLayerIds = boundaryMaps.first;
  BoundaryIdToLayerIdPair                                vBoundaryIdToLayerIds = boundaryMaps.second;

  // Verify that we have no unreferenced layers.
  for (auto &entry: hBoundaryIdToLayerIds)
  {
    BoundaryID boundaryId        = entry.first;
    LayerID    layerIdLeftPlus1  = entry.second.first;
    LayerID    layerIdRightPlus1 = entry.second.second;
    CHECK(layerIdLeftPlus1 == 0 || layerIdRightPlus1 == 0,
          "invalid layer grid configuration - unreferenced layer(s), at least with horizontal boundary ID " + boundaryId);
  }
  for (auto &entry: vBoundaryIdToLayerIds)
  {
    BoundaryID boundaryId         = entry.first;
    LayerID    layerIdTopPlus1    = entry.second.first;
    LayerID    layerIdBottomPlus1 = entry.second.second;
    CHECK(layerIdTopPlus1 == 0 || layerIdBottomPlus1 == 0,
          "invalid layer grid configuration - unreferenced layer(s), at least with vertical boundary ID " + boundaryId);
  }
}
