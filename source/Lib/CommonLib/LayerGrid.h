/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     LayerGrid.h
    \brief    layer grid configuration data structures
*/

#ifndef __LAYER_GRID__
#define __LAYER_GRID__

#include<set>
#include "Picture.h"

using namespace std;

typedef uint16_t                                BoundaryID;
typedef uint32_t                                LayerID;
typedef map<LayerID, Size>                      ChannelSizes;
typedef map<LayerID, Position>                  ChannelPositions;
typedef map<BoundaryID, pair<LayerID, LayerID>> BoundaryIdToLayerIdPair;
typedef set<Picture *>                          PicSet;
typedef map<LayerID, Picture *>                 LayerIdToPicture;
typedef map<LayerID, PicList>                   LayerIdToPicList;
typedef map<LayerID, set<Picture *>>            LayerIdToPicSet;

class PieceBoundaryConfiguration
{
private:
  BoundaryID boundaryIdNorth = 0;
  BoundaryID boundaryIdEast  = 0;
  BoundaryID boundaryIdSouth = 0;
  BoundaryID boundaryIdWest  = 0;
  bool       isChanged       = false;

public:
  BoundaryID getBoundaryIdNorth() { return boundaryIdNorth; }
  BoundaryID getBoundaryIdEast() { return boundaryIdEast; }
  BoundaryID getBoundaryIdSouth() { return boundaryIdSouth; }
  BoundaryID getBoundaryIdWest() { return boundaryIdWest; }
  bool       getIsChanged() { return isChanged; }

  bool hasBoundaryData()
  {
    return boundaryIdNorth > 0 || boundaryIdEast > 0 || boundaryIdSouth > 0 || boundaryIdWest > 0;
  }

  void setBoundaryIds(BoundaryID n, BoundaryID e, BoundaryID s, BoundaryID w)
  {
    boundaryIdNorth = n;
    boundaryIdEast  = e;
    boundaryIdSouth = s;
    boundaryIdWest  = w;
    isChanged       = true;
  }
  void resetIsChanged() { isChanged = false; }
};

typedef map<LayerID, PieceBoundaryConfiguration *> LayerIdToPieceBoundaryConfiguration;




class LayerGridPiece
{
public:
  int              layerId;
  ChannelSizes     colorChannelDimensions;
  ChannelPositions colorChannelPositions;
  ChannelPositions relativeColorChannelPositions;
  LayerGridPiece * neighborNorth = nullptr;
  LayerGridPiece * neighborEast  = nullptr;
  LayerGridPiece * neighborSouth = nullptr;
  LayerGridPiece * neighborWest  = nullptr;
};

typedef map<LayerID, LayerGridPiece *> LayerIdToLayerGridPiece;



class LayerGridConfiguration
{
  ChannelSizes colorChannelDimensions;

  static void verifyPieceAdjacency(LayerIdToLayerGridPiece *pieces, LayerGridPiece *topLeftPiece);
  static pair<BoundaryIdToLayerIdPair, BoundaryIdToLayerIdPair> reverseMap(LayerIdToPieceBoundaryConfiguration);
  static bool                                                   someHaveChanged(LayerIdToPieceBoundaryConfiguration);
  static LayerIdToLayerGridPiece collectPieces(LayerIdToPieceBoundaryConfiguration, LayerIdToPicture);
  static void assemble(LayerIdToLayerGridPiece *pieces, LayerIdToPieceBoundaryConfiguration *layerIdToJBConfig,
                       BoundaryIdToLayerIdPair *hBoundaryIdToLayerIdsPlus1,
                       BoundaryIdToLayerIdPair *vBoundaryIdToLayerIdsPlus1);
  static void setRelativePositions(LayerIdToLayerGridPiece pieces, LayerGridPiece *referencePiece);
  static void setRelativePosition(LayerGridPiece *piece, LayerGridPiece *previousPiece, LayerGridPiece *referencePiece);
  static ChannelSizes findDimensionsAndReferencePosition(LayerIdToLayerGridPiece pieces, LayerGridPiece *referencePiece);
  static void         setAbsolutePositions(LayerIdToLayerGridPiece pieces, LayerGridPiece *referencePiece,
                                           ChannelSizes colorChannelDimensions);

  void reset();

  LayerIdToPieceBoundaryConfiguration m_pieceBoundaryConfigurations;

public:
  LayerGridConfiguration() : m_pieceBoundaryConfigurations() {}

  LayerIdToLayerGridPiece pieces;

  int getWidth(int colorChannel) { return colorChannelDimensions[colorChannel].width; };
  int getHeight(int colorChannel) { return colorChannelDimensions[colorChannel].height; };
  PieceBoundaryConfiguration* getPieceBoundaryConfiguration(LayerID layerId)
  {
    if (m_pieceBoundaryConfigurations.find(layerId) == m_pieceBoundaryConfigurations.end())
    {
      m_pieceBoundaryConfigurations[layerId] = new PieceBoundaryConfiguration;
    }
    return m_pieceBoundaryConfigurations[layerId];
  };

  vector<LayerID> getLayerIds()
  {
    vector<LayerID> o;
    for (auto p: m_pieceBoundaryConfigurations)
    {
      o.push_back(p.first);
    }
    return o;
  };

  void testBoundaryConfigurations();
  void finalizeIfChanged(LayerIdToPicture);
};

#endif   // __LAYER_GRID__
